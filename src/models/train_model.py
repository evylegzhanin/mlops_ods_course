import click
import logging

import pandas as pd
import torch
from torch.optim import AdamW
from transformers import AutoTokenizer, get_scheduler

from src.data.clean_dataset import prettify_df
from src.data.get_loaders import get_loaders
from src.models.model import CustomModel, train_model
from src.constants import (
    LEARNING_RATE,
    EPOCHS,
    MODEL_NAME,
    NUM_SAMPLES,
)


@click.command()
@click.argument('data_path', type=click.Path(exists=True))
@click.argument('save_model_path', type=click.Path())
def main_train_model(data_path: str, save_model_path: str):
    df = pd.read_csv(data_path, sep=';')
    print('Data is read')

    tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    df, label_to_num, num_to_label = prettify_df(df)
    df = df.sample(NUM_SAMPLES)
    print('Dataframe is prettified')

    train_dataloader, val_dataloader, num_classes = get_loaders(df, tokenizer)

    model = CustomModel(MODEL_NAME, num_labels=num_classes)
    model = model.to(device)

    num_training_steps = EPOCHS * len(train_dataloader)
    optimizer = AdamW(model.parameters(), lr=LEARNING_RATE)
    lr_scheduler = get_scheduler(
        name="linear",
        optimizer=optimizer,
        num_warmup_steps=0,
        num_training_steps=num_training_steps
    )

    print('Start model training')
    model = train_model(
        model,
        train_dataloader,
        val_dataloader,
        num_training_steps,
        device,
        EPOCHS,
        optimizer,
        lr_scheduler,
        )

    torch.save(model.state_dict(), save_model_path)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main_train_model()
