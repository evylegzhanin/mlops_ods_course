import numpy as np
import torch
import torch.nn.functional as F

from sklearn.metrics import classification_report
from torch.optim import AdamW
from torch.utils.data import DataLoader
from tqdm.auto import tqdm
from transformers import AutoConfig, AutoModel, get_scheduler


class CustomModel(torch.nn.Module):
    """Create BERT model with linear layer"""
    def __init__(self, model_name, num_labels):
        super().__init__()
        self.config = AutoConfig.from_pretrained(model_name)
        self.transformer = AutoModel.from_pretrained(model_name, config=self.config)
        num_hidden_size = self.transformer.config.hidden_size
        self.classifier = torch.nn.Linear(num_hidden_size, num_labels)

    def forward(self, input_ids, attention_mask=None):
        hidden_states = self.transformer(input_ids=input_ids, attention_mask=attention_mask)
        cls_embeds = hidden_states.last_hidden_state[:, 0, :]
        output = self.classifier(cls_embeds)
        return output, cls_embeds


def focal_loss(
    y_pred: torch.Tensor,
    y: torch.Tensor,
    alpha: float = 1.0,
    gamma: float = 2.0
) -> torch.Tensor:
    """Custom loss function for better training"""
    loss = F.cross_entropy(y_pred, y.type(torch.LongTensor), reduce=False)
    pt = torch.exp(-loss)
    f_loss = alpha * (1 - pt) ** gamma * loss
    return f_loss.mean()


def train_epoch(
        model_tmp: CustomModel,
        device: torch.device,
        train_dataloader_tmp: DataLoader,
        optimizer: AdamW,
        lr_scheduler: get_scheduler,
        progress_bar: tqdm
) -> float:
    """Training model for only one epoch"""
    total_loss_train = 0
    for train_input, train_label in train_dataloader_tmp:
        train_label = train_label.to(device)
        mask = train_input['attention_mask'].to(device)
        input_id = train_input['input_ids'].squeeze(1).to(device)
        outputs, _ = model_tmp(input_id, attention_mask=train_input["attention_mask"].to(device))
        logits = outputs
        loss = focal_loss(logits, train_label)
        total_loss_train += loss.item()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(
            parameters=model_tmp.parameters(), max_norm=1.0
        )
        optimizer.step()
        lr_scheduler.step()
        optimizer.zero_grad()
        progress_bar.update(1)
    return total_loss_train


def eval_model(
        model_tmp: CustomModel,
        device: torch.device,
        tmp_dataloader: DataLoader
) -> tuple:
    """Evaluate the model and get some metrics"""
    progress_bar = tqdm(range(len(tmp_dataloader)))
    lst_preds = torch.tensor([]).to(device)
    lst_labels = torch.tensor([]).to(device)
    lst_probas = torch.tensor([]).to(device)
    total_loss_val = 0
    for val_input, val_label in tmp_dataloader:
        val_label = val_label.to(device)
        mask = val_input['attention_mask'].to(device)
        input_id = val_input['input_ids'].squeeze(1).to(device)
        with torch.no_grad():
            outputs, _ = model_tmp(input_id, attention_mask=val_input["attention_mask"].to(device))
        loss = focal_loss(outputs, val_label)
        total_loss_val += loss.item()
        probas = outputs.softmax(dim=-1)
        lst_probas = torch.cat((lst_probas, probas))
        predictions = torch.argmax(outputs, dim=1)
        lst_preds = torch.cat((lst_preds, predictions))
        lst_labels = torch.cat((lst_labels, val_label))
        progress_bar.update(1)
    lst_preds = lst_preds.tolist()
    lst_labels = lst_labels.tolist()
    lst_probas = lst_probas.tolist()
    return total_loss_val, lst_preds, lst_labels, lst_probas


def train_model(
        model_tmp: CustomModel,
        train_dataloader_tmp: DataLoader,
        val_dataloader_tmp: DataLoader,
        num_training_steps: int,
        device: torch.device,
        epochs: int,
        optimizer: AdamW,
        lr_scheduler: get_scheduler,
):
    """Main function for model training"""
    progress_bar = tqdm(range(num_training_steps))
    model_tmp.train()
    for epoch in range(epochs):
        total_loss_train = train_epoch(
            model_tmp,
            device,
            train_dataloader_tmp,
            optimizer,
            lr_scheduler,
            progress_bar,
        )
        (
            total_loss_val,
            lst_preds_val,
            lst_labels_val,
            lst_probas_val
        ) = eval_model(model_tmp, device, val_dataloader_tmp)
        print(f'Epoch: {epoch + 1} | Train loss: {total_loss_train/len(train_dataloader_tmp)}')
        print(f'Epoch: {epoch + 1} | Val loss: {total_loss_val/len(val_dataloader_tmp)}')
    return model_tmp


def get_metrics(
        true: np.array,
        pred: np.array,
        labels: list,
        lst_nums: list
):
    """"Get metrics during and after model training"""
    print(classification_report(list(true), pred, target_names=labels, digits=2,
                                zero_division=0, labels=lst_nums, output_dict=False))
    return classification_report(list(true), pred, target_names=labels, digits=2,
                                 zero_division=0, labels=lst_nums, output_dict=True)
