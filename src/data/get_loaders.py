import pandas as pd
from sklearn.model_selection import train_test_split
from transformers import AutoTokenizer
from torch.utils.data import DataLoader

from src.data.dataset import Dataset

from src.constants import (
    VAL_SIZE,
    BATCH_SIZE_TRAIN,
    BATCH_SIZE_VAL,
)


def make_loader(df: pd.DataFrame, tokenizer: AutoTokenizer, mode='train'):
    df_tmp = df[['text', 'label_num']]
    df_tmp.columns = ['text', 'label']
    df_tmp.reset_index(drop=True, inplace=True)

    tokenized_df = Dataset(df_tmp, tokenizer)
    if mode == 'train':
        loader = DataLoader(tokenized_df, shuffle=False, batch_size=BATCH_SIZE_TRAIN)
    else:
        loader = DataLoader(tokenized_df, shuffle=False, batch_size=BATCH_SIZE_VAL)
    return loader


def get_loaders(df: pd.DataFrame, tokenizer: AutoTokenizer):
    dtf_train, dtf_val = train_test_split(
        df,
        test_size=VAL_SIZE,
        shuffle=True,
        random_state=42,
        stratify=df['label_num']
    )
    train = dtf_train.copy()
    val = dtf_val.copy()
    print('Dataframe is split')

    train_dataloader = make_loader(train, tokenizer, mode='train')
    val_dataloader = make_loader(val, tokenizer, mode='val')

    print('Loaders are ready')
    num_classes = train.label.nunique()
    return train_dataloader, val_dataloader, num_classes
