import click
import logging

from clean_dataset import create_dataset


@click.command()
@click.argument('input_filepath_pos', type=click.Path(exists=True))
@click.argument('input_filepath_neg', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(
        input_filepath_pos: str,
        input_filepath_neg: str,
        output_filepath: str
):
    """
    Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    df = create_dataset(input_filepath_pos, input_filepath_neg)
    df.to_csv(output_filepath, index=False, sep=';')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
