import pandas as pd


def create_dataset(path_pos: str, path_neg: str) -> pd.DataFrame:
    """Create dataframe from two separate files"""
    df_pos = pd.read_csv(path_pos, header=None, sep=';')
    df_pos['label'] = 1

    df_neg = pd.read_csv(path_neg, header=None, sep=';')
    df_neg['label'] = 0

    df_full = pd.concat([df_pos, df_neg], axis=0)
    df_full = df_full.loc[:, [2, 3, 'label']]
    df_full = df_full.rename({2: 'author', 3: 'text'}, axis=1)
    return df_full


def prettify_df(
        df: pd.DataFrame,
        label_to_num: dict = None,
        num_to_label: dict = None,
        mode: str = 'train'
) -> tuple:
    """make dataframe cleaner"""
    df.dropna(subset=['text'], inplace=True)
    if mode == 'train':
        df.dropna(inplace=True)
        df.reset_index(drop=True, inplace=True)
        label_to_num = {k: i for i, k in enumerate(df['label'].unique())}
        num_to_label = {v: k for (k, v) in label_to_num.items()}
    else:
        df.dropna(inplace=True)
        df.reset_index(drop=True, inplace=True)
    df["label_num"] = df['label'].replace(label_to_num)
    return df, label_to_num, num_to_label
